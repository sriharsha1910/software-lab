/* 
 * Program for Library Management
 * The input file contains the Book's TItle, Author, Publisher and edition in each line
 * 					
 */ 


import java.io.*;
import java.util.*;
import java.lang.*;

class libMan
{
  public static void main(String args[])throws IOException
  {
    BufferedReader br = new BufferedReader(new FileReader(args[0]));
    String strLine, title, author, name, edition;
    String title_n="", author_n="", name_n="";		// To take user inputs
    int count = 0, i=0, title_f = 0, name_f = 0, author_f = 0;		// to take user opinions. _f is for flags 
    while((strLine = br.readLine()) != null)
    {				// Finding total no.of lines in the file
      count = count+1;
    }
    br.close();		// close the file
    int size = (count/4);

    Book b[] = new Book[(size)];	// Create an object of class Book 

    BufferedReader br1 = new BufferedReader(new FileReader(args[0]));
    while(i<=((size)-1))
    {				// Store the input intries in an array of book objects correspondingly
      title = br1.readLine();
      author = br1.readLine();
      name = br1.readLine();
      edition = br1.readLine();
      b[i] = new Book(title, author, name, edition);
      i = i+1;
    }

    Scanner in = new Scanner(System.in);
    Scanner in1 = new Scanner(System.in);
    Scanner in2 = new Scanner(System.in);
    Scanner in3 = new Scanner(System.in);
    Scanner in4 = new Scanner(System.in);
    Scanner in5 = new Scanner(System.in);

    System.out.print("Do you know the TITLE of the book?\nYes-1\nNo-2\nEnter:");
    title_f = in.nextInt();	// title flag
    if(title_f == 1)
    {
      System.out.print("Enter the TITLE of the book : ");
      title_n = in1.nextLine();
    }

    System.out.print("Do you know the AUTHOR of the book?\nYes-1\nNo-2\nEnter:");
    author_f = in2.nextInt();	// author flag
    if(author_f == 1)
    {
      System.out.print("Enter the AUTHOR of the book : ");
      author_n = in3.nextLine();
    }

    System.out.print("Do you know the PUBLISHER of the book?\nYes-1\nNo-2\nEnter:");
    name_f = in4.nextInt();	// publisher flag
    if((name_f == 1))
    {
      System.out.print("Enter the PUBLISHER of the book : ");
      name_n = in5.nextLine();
    }
    
    System.out.println();

    if((title_f == 1) && (author_f !=1) && (name_f != 1))
    {				// If only title is given
      for(i=0; i<size; i++)
        if(title_n.matches(b[i].title))	System.out.println("Book Details"+"\n"+"TITLE: "+b[i].title+"\n"+"AUTHOR: "+b[i].author+"\n"+"PUBLISHER: "+b[i].name+"\n"+"EDITION: "+b[i].edition);
    }

    if((title_f != 1) && (author_f ==1) && (name_f != 1))
    {				// if only author is given
      for(i=0; i<size; i++)
        if(author_n.matches(b[i].author))  System.out.println("Book Details"+"\n"+"TITLE: "+b[i].title+"\n"+"AUTHOR: "+b[i].author+"\n"+"PUBLISHER: "+b[i].name+"\n"+"EDITION: "+b[i].edition);
    }

    if((title_f != 1) && (author_f !=1) && (name_f == 1))
    {				// if only publisher is given
      for(i=0; i<size; i++)
        if(name_n.matches(b[i].name))  System.out.println("Book Details"+"\n"+"TITLE: "+b[i].title+"\n"+"AUTHOR: "+b[i].author+"\n"+"PUBLISHER: "+b[i].name+"\n"+"EDITION: "+b[i].edition);
    }

    if((title_f == 1) && (author_f == 1) && (name_f != 1))
    {				// if both title & author are given
      for(i=0; i<size; i++) 
        if((title_n.matches(b[i].title)) && (author_n.matches(b[i].author)))  System.out.println("Book Details"+"\n"+"TITLE: "+b[i].title+"\n"+"AUTHOR: "+b[i].author+"\n"+"PUBLISHER: "+b[i].name+"\n"+"EDITION: "+b[i].edition);
    }

    if((title_f != 1) && (author_f == 1) && (name_f == 1))
    {				// if both author & publisher are given
      for(i=0; i<size; i++) 
        if((name_n.matches(b[i].name)) && (author_n.matches(b[i].author)))  System.out.println("Book Details"+"\n"+"TITLE: "+b[i].title+"\n"+"AUTHOR: "+b[i].author+"\n"+"PUBLISHER: "+b[i].name+"\n"+"EDITION: "+b[i].edition);
    }

    if((title_f == 1) && (author_f != 1) && (name_f == 1))
    {				// if both title & publisher are given
      for(i=0; i<size; i++) 
        if((title_n.matches(b[i].title)) && (name_n.matches(b[i].name)))  System.out.println("Book Details"+"\n"+"TITLE: "+b[i].title+"\n"+"AUTHOR: "+b[i].author+"\n"+"PUBLISHER: "+b[i].name+"\n"+"EDITION: "+b[i].edition);
    }

    if((title_f == 1) && (author_f == 1) && (name_f == 1))
    {				// if title, author & publisher are given
      for(i=0; i<size; i++) 
        if((title_n.matches(b[i].title)) && (author_n.matches(b[i].author)) && (name_n.matches(b[i].name)))  System.out.println("Book Details"+"\n"+"TITLE: "+b[i].title+"\n"+"AUTHOR: "+b[i].author+"\n"+"PUBLISHER: "+b[i].name+"\n"+"EDITION: "+b[i].edition);
    }
  }
}

/*
 * This is a class Book which contains a constructor Book
 * 	It contains four fields namely title, author, name and editiion
 */


class Book
{
  public String title, author, name, edition;
  public Book(String title, String  author, String name, String edition) {
  this.title = title;
  this.author = author;
  this.name = name;
  this.edition = edition;
  }
}
