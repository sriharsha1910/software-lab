/*
 *  Program to find no.of triplets whose sum is even
 */

#include<stdio.h>
#include<stdlib.h>

int main()
{
    int N, K, index=-1, u_min=32766, sum=0, N1=0, N2=0, swap, temp; 
    printf("Enter 'N' - no.of packets : ");
    scanf("%d",&N);
    while(N<=0)             // No.of packets must be finite
    {
        printf("N can not be zero or negative\nEnter a natural number for 'N'\nEnter : ");
        scanf("%d",&N);
    }
    int x[N];
    printf("Enter 'K' - no.of children\nK must be less than %d\nK =  ",N);
    scanf("%d",&K);
    while(K>=N)            
    {
        printf("K must be less than %d\nEnter 'K' : ",N);
        scanf("%d",&K);
    }
    printf("\nEnter no.of candies in each packet\n");
    for(int i=0; i<N; i++)
    {
        printf("x[%d] = ",(i+1));
        scanf("%d",&x[i]);
        while(x[i]<=0)
        {
          printf("No.of candies per packet must be one or more\n");
          printf("x[%d] = ",(i+1));
          scanf("%d",&x[i]);
        }
        N1 = N1+((x[i]%2==0)?1:0);      // No.of even integers
        N2 = N2+((x[i]%2!=0)?1:0);      // No.of odd integers
    }
    /*for(int i=0; i<=N-1; i++)
    {
      printf("%d\t",x[i]);
    }*/
    do
    {
        swap=0;
        for(int i=0; i<N; i++)
        {
            if(x[i+1]<x[i])
            {
                temp = x[i];
                x[i] = x[i+1];
                x[i+1] = temp;
                swap=1;
            }
        }
    }while(swap==1);
    for(int i=0; i<N-K+1; i++)
    {
      sum=0;
      for(int j=i; j<=(i+K-1); j++)
      {
        for(int l=j; l<=(i+K-1); l++)
        {
            sum=sum+((x[j]>x[l])?(x[j]-x[l]):(x[l]-x[j])); 
        }
      }
      index=(sum<u_min)?i:index;
      u_min=(sum<u_min)?sum:u_min;
    }
    printf("No.of triplets (x1, x2, x3) possible such that (x1+x2+x3) is even is given by :\n");
    printf("\n{N1*(N1-1)*(N1-2)/3!}+{N1*N2*(N2-1)/2!}\n");      // Combinatrix idea to find the total triplets
    printf("\nN1 - No.of even numbers in (x1, x2, x3,....,xN)\n");
    printf("N2 - No.of odd numbers in (x1, x2, x3,....,xN)\n");
    printf("\nNo.of possible triplets are : %d\n",((N1*(N1-1)*(N1-2))/6)+(N1*N2*(N2-1))/2);
    printf("\nMinimum Value of u is %d\n",u_min);
    printf("The division of packets among %d students is\n",K);
    printf("index=%d\n",index);
    for(int i=index; i<=index+K-1; i++)
    {
      printf("%d\t",x[i]);
    }
    printf("\n");
}
