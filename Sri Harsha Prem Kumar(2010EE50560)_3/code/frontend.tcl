#!/usr/bin/wish



frame .fr -padx 5 -pady 5
pack .fr -fill both -expand 1

ttk::style configure TButton -width B -height B -font "serif 10"

checkbutton .fr.cbox -text "Forecast" -command onClick_forecast  \
    -onvalue true -offvalue false -variable selected_f
place .fr.cbox -x 200 -y 100 

checkbutton .fr.cb -text "Past Record" -command onClick_past  \
    -onvalue true -offvalue false -variable selected_p
#.fr.cb select 
place .fr.cb -x 300 -y 100 

proc onClick_forecast {} {
    
    global selected_f

    if {$selected_f==true} {
    
        ttk::button .fr.proceed_f -text "Proceed" -command {
        
            global response            

            exec python backend.py $val $selected_f >file.txt

            #set X 300
            #set Y 300

            #set i 40

            set in [open "file.txt" r]
            #while {[gets $in line] != -1} {
                

                #expr $i+1
                #expr $X+10
                #expr $Y+10

          #}
          set response [read $in]
          label .fr.lbl4 -text $response
          place .fr.lbl4 -x 200 -y 300
          close $in
        } 
        place .fr.proceed_f -x 220 -y 140

    } else {
    
    destroy .fr.proceed_f .fr.lbl4 .fr.lb3

    }
}
 
proc onClick_past {} {

    global selected_p

    if {$selected_p==true} {
    
        wm title . checkbutton
        scale .fr.scl -orient horizontal -from 1 -to 30 \
            -length 300  -variable val -showvalue 0
        place .fr.scl -x 315 -y 150 

        label .fr.lbl -textvariable val
        place .fr.lbl -x 400 -y 170

        label .fr.lbll -text "No.of Days:"
        place .fr.lbll -x 325 -y 170
        
        ttk::button .fr.proceed_p -text "Proceed"
        place .fr.proceed_p -x 380 -y 200

    } else {
    
    destroy .fr.scl .fr.lbl .fr.lbll .fr.proceed_p

    }
}

wm title . "Weather Forecast"
wm geometry . 250x150+300+300

listbox .fr.lb  
.fr.lb insert end "Delhi" "Mumbai" "Kolkata" \
    "Hyderabad" "Chennai" "Raipur" "Lucknow" "Visakhapatnam" "Bangalore" "Bhopal" 

bind .fr.lb <<ListboxSelect>> { setLabel [%W curselection]}

place .fr.lb -x 20 -y 30 

label .fr.lbl1 -text "Cities" 
place .fr.lbl1 -x 20 -y 10

proc setLabel { idx } {

    global val    

    set val [.fr.lb get $idx]
    label .fr.lb3 -text $val
    place .fr.lb3 -x 250 -y 50
}
