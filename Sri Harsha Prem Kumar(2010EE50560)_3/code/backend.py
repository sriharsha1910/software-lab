#!/usr/bin/python

import sys
import json
import urllib2
import MySQLdb as mdb

city = sys.argv[1]
flag = sys.argv[2]

if flag == 'true':

  con = mdb.connect('localhost', 'weather', 'weather', 'weatherdb');

  with con:
    cur = con.cursor()
    cur.execute("SELECT Code FROM Cities where Name='{}'".format(city))
    code = cur.fetchone()
    response = urllib2.urlopen('http://api.wunderground.com/api/e2c6d4b41fc25d2e/forecast10day/q/zmw:'+str(code[0])+'.json')

    data = json.load(response)
    print 'DD/MM/YYYY'+'\t'+'Max Temp(in C)'+'\t'+'Min Temp(in C)'+'\t'+'Wind(in mph)'+'\t'+'Humidity'+'\n'
    count = 0
    for day in data[u'forecast'][u'simpleforecast'][u'forecastday']:
       if count == 5:
         sys.exit()
       Day = day[u'date'][u'day']
       Month = day[u'date'][u'month']
       Year = day[u'date'][u'year']
       Max = day[u'high'][u'celsius']
       Min = day[u'low'][u'celsius']
       Wind = day[u'avewind'][u'mph']
       Humidity = day[u'avehumidity']
       
       #cur.execute("INSERT INTO Climate (Code, Day, Month, Year, MaxTemp, MinTemp, WindSpeed, Humidity) VALUES('{}', {}, {}, {}, {}, {}, {}, {})" .format(code, Day, Month, Year, Max, Min, Wind, Humidity))
       
       print str(Day)+'/'+str(Month)+'/'+str(Year)+'\t'+'\t'+str(Max)+'\t'+'\t'+str(Min)+'\t'+'\t'+str(Wind)+'\t'+str(Humidity)+'%'+'\n'
       count = count+1    
