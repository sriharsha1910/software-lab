commit cff07ecca3eea45ebfa94df5e09fd63898d4a791
Author: Sri Harsha <ee5100560@ee.iitd.ac.in>
Date:   Mon Jan 20 20:44:10 2014 +0530

    String manipulator using bash

commit 9e5c81396f19e83e1c198b4d1ffe50c25a776595
Author: Sri Harsha <ee5100560@ee.iitd.ac.in>
Date:   Mon Jan 20 17:44:13 2014 +0530

    String Manipulator - Final version with commenting

commit 2f4bb3e6ec8caea5fc2cdef148b71f3487331fdc
Author: Sri Harsha <ee5100560@ee.iitd.ac.in>
Date:   Mon Jan 20 13:40:04 2014 +0530

    String manipulator program in C(using pthreads)

commit 74f76163ce3efd605da58c9f2cac6989d49fa05d
Author: Sri Harsha <ee5100560@ee.iitd.ac.in>
Date:   Fri Jan 17 11:38:34 2014 +0530

    Implement Invocation and error handling(for solution in C language)

commit 9621f7c0ba187e3c5e4f48654dab2a30e844674e
Author: Sri Harsha <ee5100560@ee.iitd.ac.in>
Date:   Thu Jan 16 22:40:41 2014 +0530

    Assignment-1 final version
