/*
	String Manipulator Program
		
		This program takes two strings s1 and s2 and gives the total embeddings of s2 in s1 by searching par alley using pthreads 
*/


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pthread.h>
#include<time.h>
#include<unistd.h>

/*
	This structure (match) is used to send data onto the thread and vice-versa
		thread_id - Id of the thread
		str1 - First string (Text string
		str2 - Second string (Pattern string)
		count[] - stores the count of occurrences of each thread at the position of their ID	
*/

struct match
{
  int thread_id;
  char *str1;
  char *str2;
  int count[25];
};

/*
		readFile() function is used to read the contents of a file into a string dynamically i.e, without giving a pre-defined size to the string
*/

char* readFile(char *filename)
{
  int str_size, read_size;
  char *buffer = NULL;
  FILE *fp = fopen(filename, "r");
  if(fp)
  {
    fseek(fp, 0, SEEK_END);	// seek the last byte of the file
    str_size = ftell(fp);	// file size i.e, offset from first to last byte
    rewind(fp);			// go back to start of the file
    buffer = (char*) malloc(sizeof(char*)*(str_size+1));	// allocate a string to hold the contents of the file
    read_size = fread(buffer, sizeof(char), str_size, fp);	// read everything at once
    buffer[str_size+1] = '\0';	// fread() doesnot append a '\0'. So add it make buffer a string
    if(str_size != read_size)
    {
      free(buffer);
      buffer = NULL;
    } 
  }
  return buffer;
}

/*
	stringPrint() is a sub-routine which is used to print the details of a thread if a part of s1 sent onto the thread matches with s2
*/

void stringPrint(char *s, int id, int c)
{
    printf("%sThread ID is-%d\t%d\n",s,id,c);
}

/*
	threadFunc() is a standard routine used for the operation of threads
		In this routine, a part of s1 and s2 are compared using strcmp() function by traversing iteratively throughout the partition and of size s2.
		When a match is detected, the counter is incremented.
		At the end, all the details are passed to stringPrint() and thread is made to exit 
*/

void *threadFunc(void *arg)
{
  struct match *m = (struct match *)arg;
  time_t t;
  int count = 0;
  if(strlen(m->str2) <= strlen(m->str1)){
  int start = 0;
  int end = strlen(m->str2);
  while(1)
  {
    char mini_partition[strlen(m->str2)];
    for(int j=0; j<end; j++)
    {
      mini_partition[j] = m->str1[j+start];
    }
    mini_partition[end] = '\0';
    if(start>=strlen(m->str1)) break;
    start=start+1;
    if(strcmp(mini_partition, m->str2) == 0)
    {
      time(&t);
      count = count+1;
    }
  }
  m->count[m->thread_id-1] = count;
  stringPrint(ctime(&t),m->thread_id,count);
  }
  pthread_exit(NULL);
}


int main(int argc, char *argv[])
{
  char *s1, *s2;
  int i, j, l, m, n1, n2, rc, start, end, total_count = 0;
  if((argc!=1) && (argc!=2) && (argc!=3))
  {	
    printf("Invalid Input\nUse %s --help to get the instructions for running the program\n",argv[0]);
    exit(-1);
  }
  if((argc == 2) && (strcmp(argv[1], "--help") == 0))
  {
    printf("You can run the program in two ways\n1)%s <s2>\n2)%s <s1> <s2>\nNOTE-<s1> or <s2> shouldn't have white spaces in them\n",argv[0],argv[0]);
    exit(-1);
  }
  if(argc == 2)
  {
    s1 = readFile("file.txt");
    s2 = argv[1];
  }
  if(argc == 3)
  {
    s1 = argv[1];
    s2 = argv[2];
  }
  n1 = strlen(s1);
  n2 = strlen(s2);
  if(n2>n1)
  {
    printf("Length of s2 must be less than or equal to s1\n");
    exit(-1);
  }
  printf("Enter 'm'-no,of threads\nNote m must be less than %.2f\nm=",(n1/(float)n2));
  scanf("%d",&m);
  if(((float)m) >= (n1/(float)n2))
  {
    printf("'m' value must be less than len(s1)/len(s2)\n");
    exit(-1);
  }
  pthread_t threads[m];
  start = 0;
  end = (n1/m);
  l = end-start+1;
  struct match var, *ptr = NULL;
  for(i=0; i<m; i++)
  {
    ptr = &var;
    char partition[l];
    for(j = 0; j<l; j++)
    {
      partition[j] = s1[j+start];
    }
    
    start = end;	// re-define the start position of the partition for next iteration
    if(i == m-2)
    {
      end = n1-1;
      l = n1-start;
    }
    else end = start+(n1/m);
    
    var.thread_id = i+1;
    var.str1 = partition;
    var.str2 = s2;
    rc = pthread_create(&threads[i], NULL, threadFunc, (void *)&var);	// creating a thread
    sleep(1);
    if(rc)
    {
      printf("ERROR: return code from pthread_create() is %d\n",rc);
      exit(-1);
    }
  }
  for(i=0; i<m; i++) total_count = total_count+var.count[i];	// acquiring the total embeddings
  printf("Total embeddings of \x1b[10m%s\x1b[0m in \x1b[1m%s\x1b[0m is : %d\n",s2,s1,total_count);
  pthread_exit(NULL);
}

