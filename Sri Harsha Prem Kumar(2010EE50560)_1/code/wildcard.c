/* Program that matches two strings where one string contains wildcard characters '?' and '*'
    '?' - one or no characters
    '*' - one or more characters
*/ 

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>

bool checkWildcard(char *checkStr);
bool checkMatch(char *s1, char *s2);

int main(int argc, char *argv[])
{
  char str1[100], str2[100];
  printf("Enter the Text String (without wildcard characters)\n");
  fgets(str1, 100, stdin);        // Takes console input with spaces within the string
  while(checkWildcard(str1))      // checks the validity of input and prompts the user to enter a valid string if invalid
  {
    printf("Invalid Input\nRe-enter tne string without wildcard characters\n");
    fgets(str1, 100, stdin);
  }
  printf("Enter the Pattern String\n");
  fgets(str2, 100, stdin);
  //Checks if the strings are matched
  if(checkMatch(str1, str2)) printf("Strings are MATCHED\n");
  else printf("Strings are NOT MATCHED\n");
}

/*  checkWildcard() checks if either a '?' or a '*' is present in the given string
     Returns true if wildcard is present
     Returns false if wildcard is not present
*/
bool checkWildcard(char *checkStr)
{
  bool query = false;
  while(*checkStr != '\0')
  {
    if((*checkStr == '?') || (*checkStr == '*')) 
    {
      query = true;
      break;
    }
    checkStr++;
  }
  return query;
}

/*  checkMatch() checks if two given strings are matched or not
     Returns true if matched
     Returns false if unmatched
          This is a recursive function.
          The breaking condition is when both the strings have reached the end of strings
          The pointers are moved by 1 if the strings are matched
          The pointers are moved correspondingly if a '?' or a '*' is encountered
*/
bool checkMatch(char *s1, char *s2)
{
  if(*s2 == '\0' && *s1 == '\0') return true;     // breaking condition 
  if(*s2 == '*' && *(s2+1) != '\0' && *(s1) == '\0') return false;    // non-matching condition
  if(*s2 == '*') return checkMatch(s1, s2+1) || checkMatch(s1+1, s2);
  if(*s2 == '?') return checkMatch(s1, s2+1) || checkMatch(s1+1, s2+1);
  if(*s2 == *s1) return checkMatch(s1+1, s2+1);       // Move ahead by 1 on matching
  return false;
}
