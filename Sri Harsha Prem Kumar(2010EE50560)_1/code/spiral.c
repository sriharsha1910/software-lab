/*
      This program takes a Matrix and its order as an input and prints the elements in Reverse Spiral Order 
      i.e, starting at the centre, the matrix is traversed completely reaching the left most corner element
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int checkInput(char N[]);

void moveLeft(int n, int arr[n][n], int *x_p, int *y_p, int pos);
void moveRight(int n,int arr[n][n], int *x_p, int *y_p, int pos);
void moveUp(int n, int arr[n][n], int *x_p, int *y_p, int pos);
void moveDown(int n, int arr[n][n], int *x_p, int *y_p, int pos);

int main(int argc, char *argv[])
{
  int i, j, n, choice, temp=1, x_pos, y_pos;
  int *x_p, *y_p;       // position pointers
  char input[128];
  printf("Enter the ORDER OF MATRIX i.e, n\nThe value of 'n' must be odd\nn=");
  while(1)
  {
    scanf("%s",input);
    if(checkInput(input) == 1)
    {
      n = atoi(input);
      break;
    }
    else if(checkInput(input) == 2)
    {
      printf("'n' has to be an odd integer\nEnter a valid number:");
    }
    else
    {
      printf("The input value of 'n' should not contain characters\nEnter a valid input\n");
    }
  }
  int M[n][n];      // Initialising the matrix M
  printf("Do you want to enter the %d elements of the matrix?\nYes-1\nNo-2\nChoice:",(n*n));
  scanf("%d",&choice);
  while(1)
  {
  if(choice==1)
  {
    printf("Enter integers only\nIf you enter characters, the program exits abruptly\n");
    for(i=0; i<n; i++)
    {
      for(j=0; j<n; j++)
      {
        printf("Enter M[%d][%d]: ",i,j);
        scanf("%d",&M[i][j]);
      }
    }
    break; 
  }
  else if(choice == 2)
  {
    for(i=0; i<n; i++)
    {
      for(j=0; j<n; j++)
      {
        M[i][j] = temp;
        temp++;
      }
    }
    break;
  }
  else 
  {
    printf("Enter a valid choice:");
    scanf("%d",&choice);
  }
  }
  printf("----------------");
  for(i=0; i<n ;i++)
  {
    printf("\n");
    for(j=0; j<n; j++)
    {
      printf("%d\t",M[i][j]);
    }
  }
  printf("\n----------------\nReverse Spiral\n");
  x_pos = (n-1)/2;      // Initialsing the current position(x co-ordinate) to centre of the Matrix M
  x_p = &x_pos;
  y_pos = (n-1)/2;      // Initialsing the current position(x co-ordinate) to centre of the Matrix M
  y_p = &y_pos;
  printf("%d  ",M[x_pos][y_pos]);
  for(i=1; i<=((n-1)/2); i++)
  {
    /*  To move in the reverse spiral manner, start moving to the left from the centre, then to down, right and up  
         The no.of steps that are to be moved is decided by 'i' which needs to be increased progressively
    */
    moveLeft(n, M, x_p, y_p, (2*i)-1);
    moveDown(n, M, x_p, y_p, (2*i)-1);
    moveRight(n, M, x_p, y_p, (2*i));
    moveUp(n, M, x_p, y_p, (2*i));
  }                                                     // By the end of this loop, the current position is at the right tops corner
  moveLeft(n, M, x_p, y_p, n-1);        // Traversal is completed by moving left by n-1 steps
  printf("\n");
}


/*  The function checkInput verifies whether the entered order of the matrix is a valid integer.
         If its valid integer and odd, return value is 1
         If its valid integer and not odd, return value is 2
         If its not a valid integer, return value is 3
*/

int checkInput(char N[])
{
  if((atoi(N) != 0) || (strcmp(N,"0")==0)) 
  {
    if((atoi(N)%2) == 0) return 2;
    else return 1;
  }
  else return 3;
}

/*  moveLeft() shifts the current position to the left by a given no.of positions 
      by decrementing y_p by 1 progressively
*/

void moveLeft(int n, int arr[n][n], int *x_p, int *y_p, int pos)
{
  
  for(int i=1; i<=pos; i++)
  {
    *y_p=(*y_p)-1;
    printf("%d  ",arr[*x_p][*y_p]);
  }
}

/*  moveUp() shifts the current position to the top by a given no.of positions 
      by decrementing x_p by 1 progressively
*/

void moveUp(int n, int arr[n][n], int *x_p, int *y_p, int pos)
{
  
  for(int i=1; i<=pos; i++)
  {
    *x_p=(*x_p)-1;
    printf("%d  ",arr[*x_p][*y_p]);
  }
}

/*  moveDown() shifts the current position to the down by a given no.of positions 
      by incrementing x_p by 1 progressively
*/

void moveDown(int n, int arr[n][n], int *x_p, int *y_p, int pos)
{
  
  for(int i=1; i<=pos; i++)
  {
    *x_p=(*x_p)+1;
    printf("%d  ",arr[*x_p][*y_p]);
  }
}

/*  moveRight() shifts the current position to the right by a given no.of positions 
      by incrementing y_p by 1 progressively
*/

void moveRight(int n, int arr[n][n], int *x_p, int *y_p, int pos)
{
  
  for(int i=1; i<=pos; i++)
  {
    *y_p=(*y_p)+1;
    printf("%d  ",arr[*x_p][*y_p]);
  }
}

