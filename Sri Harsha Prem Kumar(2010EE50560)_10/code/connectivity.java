/*
 * Connected Component Labelling Problem
 *      Solved using the One-Pass Method
 */

import java.io.*;
import java.util.*;
import java.lang.*;
class connectivity
{
    public static void main(String args[])throws IOException
    {
        int i,j,m,n,id=1;
        System.out.print("Enter the column size: ");
        Scanner in = new Scanner(System.in);
        Point.col = Integer.parseInt(in.nextLine());
        System.out.print("Enter the row size: ");
        Scanner in1 = new Scanner(System.in);
        Point.row = Integer.parseInt(in1.nextLine());
        int land[][] = new int[Point.col][Point.row];
        System.out.println("\nEnter the values of cells.\n'0'-Land\n'1'-Water Body\n");
        Scanner in2 = new Scanner(System.in);
        for(i=0;i<Point.col;i++)
        {
            for(j=0;j<Point.row;j++)
            {
                System.out.print("land["+i+"]["+j+"]=");
                land[i][j] = Integer.parseInt(in2.nextLine());
            }
        }
        int label[][] = new int[Point.col][Point.row];
        for(i=0;i<Point.col;i++)
        {
            for(j=0;j<Point.row;j++)
            {
                label[i][j] = 0;
            }
        }
        for(i=0;i<Point.col;i++)
        {
            for(j=0;j<Point.row;j++)
            {
                if((land[i][j] == 1) && (label[i][j] == 0))
                {
                    LinkedList<Point> ne = new LinkedList<Point>();     // create a stack of neighbours of the current point including it also
                    ne.push(new Point(i,j));
                    while(!ne.isEmpty())
                    {
                        // label the foreground (water bodies or cells that have 1)
                        // pop the current element first, mark it and push all its neighbours
                        Point p = ne.pop();
                        int x = p.x, y = p.y;
                        label[x][y] = id;
                        if(((x-1)>=0) && (land[x-1][y] == 1) && (label[x-1][y] == 0))     ne.push(new Point(x-1,y));
                        if(((x+1)<Point.col) && (land[x+1][y] == 1) && (label[x+1][y] == 0))     ne.push(new Point(x+1,y));
                        if(((y-1)>=0) && (land[x][y-1] == 1) && (label[x][y-1] == 0))     ne.push(new Point(x,y-1));
                        if(((y+1)<Point.row) && (land[x][y+1] == 1) && (label[x][y+1] == 0))     ne.push(new Point(x,y+1));
                    } 
                    id = id+1; 
                }

            }
        }
        Scanner in3 = new Scanner(System.in);
        Scanner in4 = new Scanner(System.in);
        System.out.println("\nThe total no.of water bodies are "+(id-1));
        System.out.print("\nEnter the cell (m,n)\nm=");
        m = Integer.parseInt(in3.nextLine());
        System.out.print("n=");
        n = Integer.parseInt(in3.nextLine());
        System.out.println("The label of the cell ("+m+","+n+") for 4-point connectivity is "+label[m][n]);
    }
}

/*
 * The class Point is used to store a cell's x and y co-ordinates of the form (x,y)
 */
class Point
{
    public static int col, row;
    public int x,y;
    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

