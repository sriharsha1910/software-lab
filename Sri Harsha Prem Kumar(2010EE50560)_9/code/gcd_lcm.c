#include<stdio.h>

int find_gcd(int m, int n)
{
  if(n==0) return m;
  else return find_gcd(n, (m%n));
}

int main()
{
  int n1,n2;
  int a,b,t,gcd,lcm;
  printf("Enter two integers\nn1 = ");
  scanf("%d",&n1);
  printf("n2 = ");
  scanf("%d",&n2);
  a=n1;
  b=n2;
  /*while(b != 0)
  {
    t = b;
    b = a % b;
    a = t;
  }*/
  gcd = find_gcd(a,b);
  lcm = (n1 * n2)/gcd;
  #ifdef DEFINED 
  printf("GCD = %d\n",gcd);
  #else
  printf("LCM = %d\n",lcm);
  #endif
}
